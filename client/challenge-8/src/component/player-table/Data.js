import e from "cors";
import React, { Fragment, useState } from "react";

const Table = ({ players, filteredPlayers, setFilteredPlayer, setPlayers }) => {
  const [isEdit, setIsEdit] = useState(false);

  const [editField, setEditField] = useState({
    id: "",
    username: "",
    email: "",
    password: "",
    experience: "",
    level: "",
  });
  const displayEdit = (id) => {
    const selectedPlayer = filteredPlayers.find((player) => player.id === id);
    const editedPlayer = { ...selectedPlayer, isEdit: true };
    console.log(editedPlayer);
    setEditField(editedPlayer);
    setIsEdit(true);
  };

const changeEditedPlayer = (editKey, value) => {
    const newFields = {...editField};
    newFields[editKey] = value
    setEditField(newFields)
}

const saveEditedPlayer = () => {
  const editedPlayer = editField
  if (!editedPlayer.username || !editedPlayer.email || !editedPlayer.password || !editedPlayer.experience || !editedPlayer.level) {
    setPlayers(players)
    setFilteredPlayer(players)
    setIsEdit(false)
    return
  } 
  const objIndex = players.findIndex((obj => obj.id == editedPlayer.id));
  players[objIndex] = editedPlayer
  setPlayers(players);
  setFilteredPlayer(players);
  setIsEdit(false)
}
  return (
    <div className="container d-inline-block col-9 align-top">
      <h2>Player List:</h2>
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">username</th>
            <th scope="col">e-mail</th>
            <th scope="col">expereince</th>
            <th scope="col">level</th>
            {isEdit ? <th scope="col">password</th> : null}
            <th scope="col">action</th>
          </tr>
        </thead>
        <tbody className="table-group-divider">
          {!isEdit ? (
            filteredPlayers.map((player, i = 0) => (
              <Fragment>
                <tr key={i}>
                  <th scope="row">{i + 1}</th>
                  <td>{player.username} </td>
                  <td>{player.email}</td>
                  <td>{player.experience}</td>
                  <td>{player.level}</td>
                  <td>
                    <button
                      onClick={() => displayEdit(player.id)}
                      className="btn btn-warning"
                    >
                      edit
                    </button>
                  </td>
                </tr>
              </Fragment>
            ))
          ) : (
            <Fragment>
                <td>
                #
              </td>
              <td>
                <input type="text" value={editField.username} onChange= {e => changeEditedPlayer("username", e.target.value)}/>
              </td>
              <td>
                <input type="text" value={editField.email} onChange= {e => changeEditedPlayer("email", e.target.value)} />
              </td>
              <td>
                <input type="text" value={editField.experience} onChange= {e => changeEditedPlayer("experience", e.target.value)} />
              </td>
              <td>
                <input type="text" value={editField.level} onChange= {e => changeEditedPlayer("level", e.target.value)} />
              </td>
              <td>
                <input type="text" value={editField.password} onChange= {e => changeEditedPlayer("password", e.target.value)} />
              </td>
              <td>
                <button className="btn btn-warning" onClick={() => saveEditedPlayer()}>save</button>
                <button
                  className="btn btn-warning"
                  onClick={() => setIsEdit(false)}
                >
                  cancel
                </button>
              </td>
            </Fragment>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
