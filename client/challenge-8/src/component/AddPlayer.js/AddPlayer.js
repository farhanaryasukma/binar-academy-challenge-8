import React from "react";

const AddPlayer = ({setDisplay, createPlayer, newPlayer, addNewPlayer}) => {
  return (
    <div className="display-add-player container col-6 mb-5">
      <h2>Add Player:</h2>
      <div className="mb-2">
        <label className="form-label" htmlFor="username">
            username
        </label>
        <input className="form-control" type="text" value={newPlayer.username} onChange={e => createPlayer("username", e.target.value)} />
      </div>
      <div className="mb-2">
        <label className="form-label" htmlFor="username">
          e-mail
        </label>
        <input className="form-control" type="email" value={newPlayer.email} onChange={e => createPlayer("email", e.target.value)}/>
      </div>
      <div className="mb-2">
        <label className="form-label" htmlFor="username">
          password
        </label>
        <input className="form-control" type="password" value={newPlayer.password} onChange={e => createPlayer("password", e.target.value)}/>
      </div>
      <button className="btn btn-primary d-inline-block " onClick={addNewPlayer}>Add</button>
      <button className="btn btn-primary d-inline-block float-end" onClick={() => setDisplay("hilang")}>cancel</button>
    </div>
  );
};

export default AddPlayer;
