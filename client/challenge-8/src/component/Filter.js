import React, { useState } from "react";

const Filter = ({ players, filteredPlayers, setFilteredPlayer }) => {
  const [filter, setFilter] = useState({
    username: "",
    email: "",
    experience: "",
    level: "",
  });

  const filterValue = (key, value) => {
    const playerFilter = { ...filter };
    playerFilter[key] = value;

    setFilter(playerFilter);
  };
  const runFilter = () => {
    const filterKey = filter;
    if (
      !filterKey.username &&
      !filterKey.email &&
      !filterKey.experience &&
      !filterKey.level
    ) {
      setFilteredPlayer(players);
      return;
    }

    const playerFilters = filteredPlayers.filter((player) => {
      if (filterKey.username && player.username !== filterKey.username)
        return false;
      if (filterKey.email && player.email !== filterKey.email) return false;
      if (filterKey.experience && player.experience !== filterKey.experience)
        return false;
      if (filterKey.level && player.level !== filterKey.level) return false;
      return true;
    });
    setFilteredPlayer(playerFilters);
  };

  return (
    <div className="filter d-inline-block col-3">
      <h2>Filter</h2>
      <div className="mb-2">
        <label className="form-label">username</label>
        <input
          className="form-control"
          type="text"
          value={filter.username}
          onChange={(e) => filterValue("username", e.target.value)}
        />
      </div>
      <div className="mb-2">
        <label className="form-label">e-mail</label>
        <input
          className="form-control"
          type="text"
          value={filter.email}
          onChange={(e) => filterValue("email", e.target.value)}
        />
      </div>
      <div className="mb-2">
        <label className="form-label">experience</label>
        <input
          className="form-control"
          type="number"
          value={filter.experience}
          onChange={(e) => filterValue("experience", parseInt(e.target.value))}
        />
      </div>
      <div className="mb-2">
        <label className="form-label">level</label>
        <input
          className="form-control"
          type="number"
          value={filter.level}
          onChange={(e) => filterValue("level", parseInt(e.target.value))}
        />
      </div>
      <button className="btn btn-primary" onClick={() => runFilter()}>
        search
      </button>
    </div>
  );
};

export default Filter;
