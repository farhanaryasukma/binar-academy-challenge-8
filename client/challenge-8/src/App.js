import "./App.css";
import Filter from "./component/Filter";
import "bootstrap/dist/css/bootstrap.min.css";
import Table from "./component/player-table/Data";
import { useState } from "react";
import AddPlayer from "./component/AddPlayer.js/AddPlayer";

function App() {
  const [players, setPlayers] = useState([
    {
      id: 1,
      username: "a",
      email: "a@gmail.com",
      password: "abc",
      experience: 600000,
      level: 600,
    },
    {
      id: 2,
      username: "b",
      email: "b@gmail.com",
      password: "abc",
      experience: 666000,
      level: 666,
    },
    {
      id: 1,
      username: "a",
      email: "aa@gmail.com",
      password: "abc",
      experience: 600000,
      level: 600,
    },
  ]);

  const [id, setId] = useState(3);

  const [filteredPlayer, setFilteredPlayer] = useState(players);

  const [display, setDisplay] = useState("hilang");

  const [newPlayer, setNewPlayer] = useState({
    username: "",
    email: "",
    password: "",
    experience: 0,
    level: 0,
  });

  const createPlayer = (key, value) => {
    const createdPlayer = { ...newPlayer };
    createdPlayer[key] = value;

    setNewPlayer(createdPlayer);
  };
  const addNewPlayer = () => {
    setId(id + 1);
    const playerId = id;
    const addedPlayer = { id: playerId, ...newPlayer };
    const newPlayers = [...players, addedPlayer];
    setPlayers(newPlayers);
    setFilteredPlayer(newPlayers);
    setNewPlayer({
      username: "",
      email: "",
      password: "",
      experience: 0,
      level: 0,
    });
    setDisplay("hilang");
  };

  return (
    <div className="App container mt-5">
      <h1>Challenge-8 Dashboard</h1>
      <button
        className="btn-add-player ms-auto d-block"
        onClick={() => setDisplay("tampil")}
      >
        add player
      </button>
      <div className="content pt-2">
        {display === "tampil" ? (
          <AddPlayer
            setDisplay={setDisplay}
            newPlayer={newPlayer}
            createPlayer={createPlayer}
            addNewPlayer={addNewPlayer}
          />
        ) : null}
        {display === "hilang" && <Filter players={players} filteredPlayers={filteredPlayer} setFilteredPlayer= {setFilteredPlayer} />}
        {display === "hilang" && <Table players={players} setPlayers={setPlayers} filteredPlayers={filteredPlayer} setFilteredPlayer={setFilteredPlayer} />}
      </div>
    </div>
  );
}

export default App;
